#!/bin/bash

path="public_html"


help="\nHelp: This script is used to fix permissions of a Drupal 8 installation in which the PHP Handler is FastCGI\nYou need to provide the following arguments:\n\t 1) Path to your drupal installation\n\t 2) Username of the user that you want to give files/directories ownership\n\nNote: This script assumes that the group is the same as the username if your PHP is compiled to run as FastCGI. If this is different you need to modify it manually by editing this script or checking out forks on GitHub.\nUsage: bash ${0##*/} drupal_path user_name\n"

echo "Refer to https://www.Drupal.org/node/244924"

if [ -z "public_html" ] || [ ! -d "public_html/sites" ] || [ ! -f "public_html/core/core.api.php" ]; then
        echo "Please provide a valid drupal path"
        echo -e $help
        exit
fi

cd $path;
echo "Changing permissions of all directories inside "public_html" to "755"..."
find . -type d -exec chmod u=rwx,go=rx {} \;
echo -e "Changing permissions of all files inside "public_html" to "644"..."
find . -type f -exec chmod u=rw,go=r {} \;

cd sites ;
echo "Changing permissions of "files" directories in "public_html/sites" to "775"..."
find . -type d -name files -exec chmod ug=rwx,o=rx '{}' \;
echo "Changing permissions of all files inside all "files" directories in "public_html/sites" to "664"..."
find . -name files -type d -exec find '{}' -type f \; | while read FILE; do chmod ug=rw,o=r "$FILE"; done
echo "Changing permissions of all directories inside all "files" directories in "${path}/sites" to "775"..."
find . -name files -type d -exec find '{}' -type d \; | while read DIR; do chmod ug=rwx,o=rx "$DIR"; done


cd ../../vendor;
echo "Changing permissions of all directories inside vendor to "755"..."
find . -type d -exec chmod u=rwx,go=rx {} \;
echo -e "Changing permissions of all files inside vendor to "644"..."
find . -type f -exec chmod u=rw,go=r {} \;
chmod 775 -R drush/

cd ../scripts;
echo "Changing permissions of all directories inside scripts to "755"..."
find . -type d -exec chmod u=rwx,go=rx {} \;
echo -e "Changing permissions of all files inside scripts to "644"..."
find . -type f -exec chmod u=rw,go=r {} \;

cd ../config;
echo "Changing permissions of all directories inside config to "755"..."
find . -type d -exec chmod u=rwx,go=rx {} \;
echo -e "Changing permissions of all files inside config to "644"..."
find . -type f -exec chmod u=rw,go=r {} \;
